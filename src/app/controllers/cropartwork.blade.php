@extends('layouts.master')

@section('title')
@parent
 :: {{ $title }}
@stop

@section('css')
@parent
<link rel="stylesheet" href="/assets/css/cropper.css">
@stop

@section('content')
<div class="container eg-container" id="basic-example">
	<div class="row">
		<div class="col-md-9 eg-main">
			<h3>Crop Your Image</h3>
			<div class="eg-wrapper">
				  <img class="cropper" src="{{ $artwork }}" alt="Picture">
			</div>
		</div>
		<div class="col-md-3">
			<h3>Preview Image</h3>
		    <div class="eg-preview clearfix" id="preview-wrapper">
		    	<div id="message-holder"><span id="mymessagepreview">Your Message</span></div>
				<div id="overlaygraphic"></div>
				<div class="preview preview-lg"></div>
		    </div>
		    <form role="form" id="messagetxt" method="post" action="/processartwork">
		    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    	<div class="form-group">
		    		<label for="mymessage">You Drive For:</label>
		    		<input class="form-control" type="text" id="mymessage"
		    			name="mymessage" placeholder="Your Message Here" value="">
		    	</div>
		    	<div class="form-group">
			    	<label for="whichfont">Which Font?</label>
			    	<select name="whichfont" id="whichfont" class="form-control" style="font-family: 'Permanent Marker', cursive;">
					    <option value="PermanentMarker.ttf"
					    	data-fontname="Permanent Marker" selected
					    	>Permanent Marker</option>
					    <option value="Handlee-Regular.ttf"
					    	data-fontname="Handlee"
					    	>Handlee</option>
					    <option value="NothingYouCouldDo.ttf"
					    	data-fontname="Nothing You Could Do"
					    	>Nothing You Could Do</option>
					</select>
		    	</div>
		    	<input id="img-src" name="img_src" type="hidden" value="{{ $artwork }}">
				<input id="img-data" name="img_data" type="hidden">
				<div class="form-group">
					<button type="submit"
						class="btn btn-primary btn-large pull-right">Save It!</button>
				</div>
		    </form>
		</div>
	</div>
</div>
@stop

@section('scripts')
@parent
<script src="/assets/js/docrop.js"></script>
@stop
